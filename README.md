projectdoc for Java Developers
===============================

## Overview

This extension provides a space blueprint to create a space based on a [Maven POM](https://maven.apache.org/pom.html). The POM information is added as Metadata documents and their information is made available as space properties. All Metadata documents (currently it is only one that contains all POM information) are added as subpages to a Version document. This makes it easy to switch versions for the documentation of a space and its references to resources on remote servers (such as [Nexus](http://www.sonatype.com/nexus/product-overview) or [Javadoc reports](http://www.oracle.com/technetwork/articles/java/index-jsp-135444.html) on a [Maven site](https://maven.apache.org/plugins/maven-site-plugin/)).

## Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc for Java Developers Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/projectdoc+for+Java+Developers)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-java) - available soon

Related doctype add-ons on Bitbucket:

  * [projectdoc for Maven Developers](https://bitbucket.org/smartics-de/smartics-projectdoc-confluence-space-maven)
  * [projectdoc Doctypes for Software Development](https://bitbucket.org/smartics-de/smartics-projectdoc-confluence-space-swdev)
  * [projectdoc Add-on for arc42](https://bitbucket.org/smartics-de/smartics-projectdoc-confluence-arc42)
