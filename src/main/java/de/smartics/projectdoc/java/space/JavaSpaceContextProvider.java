/*
 * Copyright 2015-2024 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.java.space;

import com.atlassian.confluence.plugins.createcontent.TemplateRendererHelper;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.extension.maven.projects.ArtifactRepositoryService;
import de.smartics.projectdoc.extension.maven.projects.MavenSpaceContextProvider;

/**
 * Provides information from a Maven POM for the space.
 */
public class JavaSpaceContextProvider extends MavenSpaceContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public JavaSpaceContextProvider(TemplateRendererHelper templateRendererHelper,
      final ContextProviderSupportService support,
      final ArtifactRepositoryService artifactRepositoryService) {
    super(templateRendererHelper, support, artifactRepositoryService);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------


  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    return super.updateBlueprintContext(blueprintContext);
  }

  // --- object basics --------------------------------------------------------

}
